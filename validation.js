const supportedFileTypes = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

module.exports = function fileTypeValidation (filename) {
    const type = filename.split('.')[filename.split('.').length - 1].toLowerCase()
    return supportedFileTypes.filter(i => i === type).length === 1
}
