const express = require('express')
const fs = require('fs')
const path = require('path')
const fileTypeValidation = require('./validation')

const filesDir = path.join(__dirname, 'files')
let isFilesDir = fs.existsSync(filesDir)


const app = express()
app.use(express.json())
app.use((req, res, next) => {
    let now = new Date()
    let hour = now.getHours()
    let minutes = now.getMinutes()
    let seconds = now.getSeconds()
    let data = `${hour}:${minutes}:${seconds} ${req.method} ${req.url}`
    fs.appendFile("server-log.txt", data + "\n", () => {
    })
    next()
})


app.post('/api/files', (req, res) => {
    const {filename, content} = req.body
    if (!isFilesDir) {
        fs.mkdirSync(filesDir)
    } else {
        if (fs.existsSync(path.join(filesDir, filename))) {
            return res.status(400).json({message: 'File already exists'})
        }
        if (!filename) {
            return res.status(400).json({message: 'Please specify \'filename\' parameter'})
        }
        if (!fileTypeValidation(filename)) {
            return res.status(400).json({
                message: 'Type of your file doesn\'t supported. Application supports next file types: log, txt, json, yaml, xml, js'
            })
        }
        if (!content) {
            return res.status(400).json({message: 'Please specify \'content\' parameter'})
        }
    }
    fs.writeFile(path.join(filesDir, filename), content, () => {
        res.status(200).json({message: 'File created successfully'})
    })


})

app.get('/api/files', (req, res) => {
    const allFiles = []
    if (!isFilesDir) {
        return res.status(400).json({message: 'No such folder exists'})
    } else {
        fs.readdir(filesDir, (err, files) => {
            files.forEach(file => {
                allFiles.push(file)
            })
            res.json({
                message: 'Success',
                files: allFiles
            })
        })
    }


})

app.get('/api/files/:fileName', (req, res) => {
    let {fileName} = req.params
    let filePath = path.join(filesDir, fileName)
    const dirFilesArray = fs.readdirSync(filesDir, 'utf-8')
    const file = dirFilesArray.find(el => fileName === el)

    if (!file) {
        return res.status(400).json({message: `No file with '${fileName}' filename found`})
    } else {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            let uploadedDate = fs.statSync(filePath).birthtime
            if (err) {
                return res.status(400).json({message: `Client error`})
            }
            res.json({
                message: "Success",
                filename: fileName,
                content: data,
                extension: path.extname(fileName),
                uploadedDate
            })
        })
    }
})

app.delete('/api/files/:fileName', (req, res) => {
    let {fileName} = req.params
    let filePath = path.join(filesDir, fileName)
    const dirFilesArray = fs.readdirSync(filesDir, 'utf-8')
    const file = dirFilesArray.find(el => fileName === el)

    if (!file) {
        return res.status(400).json({message: `No file with '${fileName}' filename found`})
    } else {
        fs.unlink(filePath, () => {
            console.log('successfully deleted /tmp/hello');
            return res.status(200).json({message: `File '${fileName}' has been deleted`})
        })
    }
})

app.use('/*', (req, res) => {
        return res.status(400).json({message: 'Client error'})

})

app.listen(8080)


